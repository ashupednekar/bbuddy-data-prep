import os

print('Current directory is', os.system('pwd'))
os.chdir('audio')
print('Directory changed to audio...')
mp3files = os.listdir()

print(mp3files[0])
os.makedirs('converted')
for mp3 in mp3files:
    mp3 = mp3.replace(' ', '\ ')
    print('=======================')
    print(mp3)
    os.system('ffmpeg -i '+mp3+' -acodec pcm_s16le -ac 1 -ar 16000 '+ 'converted/'+ mp3.split('.')[0]+'.wav')
    print(mp3.split('.')[0]+'.wav generated...')

