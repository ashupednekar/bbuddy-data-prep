from aeneas.executetask import ExecuteTask
from aeneas.task import Task
import os

print('Initial directory: ', os.getcwd())
print('Changing directory...')
os.chdir('audio')
print('Current directory is: ', os.getcwd())
speeches = os.listdir('.')
for s in speeches:
    s = 'audio/'+str(s)

print('Going back to project root directory.')
os.chdir('..')

print('Current directory: ', os.getcwd())
print('Changing directory...')
os.chdir('aeneas_text')
print('Current directory is: ', os.getcwd())
texts = os.listdir('.')
for t in texts:
    t = 'audio/'+str(t)

# create Task object
config_string = 'task_language=swe|is_text_type=plain|os_task_file_format=json'
task = Task(config_string=config_string)

print(speeches[0])
print(texts[0])

for s in speeches:
    for t in texts:
        task.audio_file_path_absolute = s
        task.text_file_path_absolute = t
        task.sync_map_file_path_absolute = 'json_outputs/' + t + '/syncmap.json'
        # process Task
        ExecuteTask(task).execute()
        # output sync map to file
        task.output_sync_map_file()
