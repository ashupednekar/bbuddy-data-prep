from pydub import AudioSegment
import pandas as pd
import json
import os

if not os.getcwd() == '/home/ashu/Documents/bbuddy-data-prep':
    print('Please run this file from the project root.')

speeches = os.listdir('audio')[:500]

#Creation of the placeholder dataframe.
df = pd.DataFrame(columns=['filename', 'text', 'up_votes', 'down_votes', 'age', 'gender', 'accent', 'duration'])

for speech in speeches:
    #Load the audio and the syncmap created previously.
    s = AudioSegment.from_mp3(speech)
    try:
        with open('json_outputs_dev/'+speech+'/syncmap.json') as fl:
            syncmap = json.loads(fl.read())
    except Exception as e:
        print(e)
        #This part wouln't really matter in our case, since our use case is for short sentences...
    sentences = []
    #Loop through all the segments/fragments/sentences in the syncmap
    for fragment in syncmap['fragments']:
        if ((float(fragment['end']) * 1000) - float(fragment['begin']) * 1000) > 400:
            sentences.append({"audio":book[float(fragment['begin'])*1000:float(fragment['end'])*1000], "text":fragment["lines"][0]})

    # export audio segment
    for idx, sentence in enumerate(sentences):
        text = sentence['text'].lower()
        sentence['audio'].export('books / audio_output / sample -'+str(idx) +'.mp3', format='mp3')
        accent = speech.split('_')[1] # + '_' + speech.split('_')[2]
        temp_df = pd.DataFrame([{'filename':'sample -'+str(idx) +'.mp3', 'text':text, 'up_votes':0, 'down_votes':0,'age':0,'gender':'male','accent': accent ,'duration':''}], columns=['filename', 'text', 'up_votes', 'down_votes', 'age', 'gender', 'accent', 'duration'])
        df = df.append(temp_df)


print(df.head())
