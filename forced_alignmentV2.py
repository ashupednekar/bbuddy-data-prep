from aeneas.executetask import ExecuteTask
from aeneas.task import Task
import os

speeches = os.listdir('audio')[:500]

# create Task object
config_string = 'task_language=en|is_text_type=plain|os_task_file_format=json'

task = Task(config_string=config_string)
print(speeches)

# os.chdir('audio')
print('Directory changed to: ', os.getcwd())

i = 0
for speech in speeches:
    s = '/home/ashu/Documents/bbuddy-data-prep/audio/' + speech
    t = '/home/ashu/Documents/bbuddy-data-prep/aeneas_text/' + speech + '.txt'
    try:
        task.audio_file_path_absolute = s
        task.text_file_path_absolute = t
        task.sync_map_file_path_absolute = 'json_outputs_dev/' + speech + '/syncmap.json'
        print('processing task...')
        ExecuteTask(task).execute()
        print('Dumped sync map to files...')
        i = i + 1
        print('------------------------'+str(i)+'-------------------------')
        task.output_sync_map_file()
    except Exception as e:
        print(e)


